<?php

/**
 * @file
 * Drush integration for rsc_webpush.
 *
 * Adapted from the Kint module.
 */

/**
 * Implements hook_drush_command().
 */
function rsc_webpush_drush_command() {
  $items = [];

  // The key in the $items array is the name of the command.
  $items['rsc-webpush-download'] = [
    'callback' => 'drush_rsc_webpush_download',
    'description' => dt("Downloads the required libraries to sites/all/libraries."),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_SITE,
  ];

  return $items;
}

/**
 * Implements hook_drush_help().
 */
function rsc_webpush_drush_help($section) {
  switch ($section) {
    case 'drush:rsc-webpush-download':
      return dt("Downloads the required libraries to sites/all/libraries.");
  }
}

/**
 * Command to download the Web Push PHP library.
 */
function drush_rsc_webpush_download() {
  if (!drush_confirm(dt('Do you want to download the required libraries for RSC Web Push?'))) {
    // Nothing to do.
    return;
  }

  // Make sure the libraries path exists.
  $libraries_path = 'sites/all/libraries';
  if (!is_dir($libraries_path)) {
    drush_op('mkdir', $libraries_path);
    drush_log(dt('Directory @path was created', ['@path' => $libraries_path]), 'notice');
  }

  // Set the directory to the download location.
  $olddir = getcwd();
  chdir($libraries_path);

  foreach (rsc_webpush_libraries_info() as $library_name => $library_info) {

    // Skip existing libraries.
    if (is_dir($library_name)) {
      drush_log(dt('Library @name already exists at @path', [
        '@path' => $libraries_path,
        '@name' => $library_name,
      ]), 'success');
      continue;
    }

    // Download the zip archive
    $filepath = drush_download_file($library_info['download url']);
    if ($filepath) {
      $filename = basename($filepath);

      // Make sure that the folder we'll extract to does not exist.
      if (is_dir($library_info['extracted name'])) {
        drush_delete_dir($library_info['extracted name'], TRUE);
      }

      // Decompress the zip archive.
      drush_tarball_extract($filename);

      // Make the folder the same as the library name.
      drush_move_dir($library_info['extracted name'], $library_name, TRUE);
    }

    if (is_dir($library_name)) {
      drush_log(dt('Library @name installed at @path', [
        '@path' => $libraries_path,
        '@name' => $library_name,
      ]), 'success');
    }
    else {
      drush_log(dt('Unable to install library @name to @path', [
        '@path' => $libraries_path,
        '@name' => $library_name,
      ]), 'error');
    }

  }

  // Set working directory back to the previous working directory.
  chdir($olddir);
}

/**
 * Take action after a project has been downloaded.
 */
function drush_rsc_webpush_post_pm_enable() {
  $args = func_get_args();

  // reflect only on enabling of RSC Web Push PHP module
  if (in_array('rsc_webpush', $args)) {
    drush_rsc_webpush_download();
  }
}
