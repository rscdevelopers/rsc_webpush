core = 7.x
api = 2

; Libraries
; ---------
libraries[rsc_webpush][type] = "libraries"
libraries[rsc_webpush][download][type] = "file"
libraries[rsc_webpush][download][url] = "https://github.com/web-push-libs/web-push-php/archive/v1.4.3.zip"
libraries[rsc_webpush][directory_name] = "rsc_webpush"
libraries[rsc_webpush][destination] = "libraries"
