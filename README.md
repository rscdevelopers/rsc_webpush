# RSC Web Push

This module makes it easy to pull in the Web Push PHP library with its dependencies into Drupal 7.
It also integrates with `drush en` and `drush make`.
