<?php

namespace rsc_webpush\exceptions;

/**
 * Class LibraryException.
 *
 * Thrown when we fail to load a library.
 */
class LibraryException extends \RuntimeException {}
